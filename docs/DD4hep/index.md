# DD4hep cheatsheet

DD4hep is the software toolkit that we use to implement detector geometries. A typical DD4hep detector implementation has a C++ detector builder where the shapes, volumes and placedVolumes are defined and an xml 'compact file' that configures the detector (e.g. setting the dimensions) and from which the C++ builder retrieves all the free parameters. When implementing a detector, the philosphy should be to hide all the complexity in the C++ and make the comapct file as simple as possible (and well commented!). The ultimate target being that, upon a change of the detector envelope dimensions in the xml (plus possibly another handful of well explained parameters), the code automatically applies all the sub-sequent changes needed to have a valid detector. The latter is of course not always possible, especially for detailed descriptions including services or support structures, but we should try to get as close as we can from this.

Here are useful links to available DD4hep documentation:

- [Beginners guide](https://dd4hep.web.cern.ch/dd4hep/page/beginners-guide/)
- [DD4hep basics](https://codimd.web.cern.ch/s/n78QRlv1p#)
- [DD4hep manual](https://dd4hep.web.cern.ch/dd4hep/page/users-manual/)
- [DD4hep Doxygen page](https://dd4hep.web.cern.ch/dd4hep/reference/), useful for development
- [Changing the geometry tutorial](https://hep-fcc.github.io/fcc-tutorials/master/full-detector-simulations/Geometry/Geometry.html)

And here are some important points for the impatient user:

- In the xml compact file, the detector builder to use is specified by the `type` keyword in the `detector` beacon (e.g. `<detector id=1 name="MyCoolDetectorName" type="MYCOOLDETECTOR" ... />`) and it should match the detector type defined in the C++ builder with the instruction `DECLARE_DETELEMENT(MYCOOLDETECTOR)`. For any other reference to this detector, use its `name` instead ("MyCoolDetectorName" in this example).
- The Geant4 sensitive action (what defines how Geant4 step information is stored) is set in the C++ builder with the `setSensitiveDetector` member of the `dd4hep::Volume` class but could be retrieved from the xml.  
- ...(to be continued)

## Implementing detector geometries

The main components of a geometry tree are the following:
* Shape: mathematical description of a volume.
* Volume: object defined by a shape and a material. 
* Placed Volume: object defined by a volume and a geometrical transformation (position+rotation) in the reference system of the mother voluem where it is placed. The same volume can be placed many times. The world volume is the top volume in the geometry tree, and it is provided by DD4hep.

The so-called Detector tree is another tree structure linked to the geometry tree. This tree is made up by detector element objects, which corresponds to a Placed Volume from the geometry tree. At least one detector element must be defined in the detector constructor, and assigned to the whole subdetector volume. This tree might be optional in a first stage of the geometry implementation. The detector tree may simplyfy the navigation in the geometry tree for the user. In addition, it allows the application of features such aligment and conditions to the volumes that are associated to detector elements (nodes of the detector tree).

### General considerations

* Each subdetector is expected to be contained in an envelope volume. This volume may be made of air as the world. 
* Because each subdetector is contained in one volume, endcaps and barrel of a given subdetector may be built by different detector constructors. Despite being built by different detector constructors, they can use the same readout.
* Different little pieces (volumes) of the detector may be grouped into bundles according to symmetry. 
	- For example, if there is a symmetry around Z-axis (phi), intermediate envelopes (e.g., named sectors) which will contain all the daughter volumes, may be defined and placed many times around Z-axis.
	- If there is radial symmetry (that is, the same layer placed several times), an intermediate envelope volume which contains a layer (and all the sub-volumes) can be created and placed as many times as needed
	- Deeper grouping may be done if symmetry allows it
* Different little pieces (volumes) of the detector may be grouped into a module, tile, cell, etc, something one could hold on the hand. Then, this module/tile/cell can be placed many times as needed. Example, an detector might be made of tiles, each tile containing a gas, electronics, and other components. This tiles can be the fundamental unit, to be grouped into intermediate envelopes as described in the previous point.
* Physical volume ID is assigned to all daughter volumes. If volume “sector_1” has “Phi” bitfield “1”, all daughters will have that bitfield set to “1”.
* Linking Placed Volumes to DD4hep detector elements may be done with care, the detector tree may have to follow similar structure to the geometry tree.

Some tips and frequent issues:

* In the detector constructor C++ function, please create volume, shape, etc, objects outside the loops that will iterate over phi/theta/R if possible.
* Please, keep the number of daughter volumes below o(1000). Use intermediate envelope volumes to reduce this number. This will speed up geometry construction and navigation.
* Please, do not abuse the assembly-type volume. During translation of geometry, all daughters are placed directly into the grand-mother volume, assembly does not exist during the simulation.
* Please think twice before using shapes such as Boolean operations or tessellated solids, they will impact performance

# DDSIM cheatsheet

The role of `ddsim` is to allow you to configure the Geant4 simulation through command line or through a Python steering file. The configuration of the default parameters can be printed out as in the following

```
source /cvmfs/sw-nightlies.hsf.org/key4hep/setup.sh
ddsim --dumpSteeringFile
ddsim --dumpSteeringFile > mySteeringFile.py
# or ddsim -h
```

## General features

Geant4 provides different User Interfaces, which are grouped into
* batch mode (types: run, batch) the simulation is executed without waiting for user input. UI commands can be fed from a macro file.
* interactive mode (types: qt,shell): Geant4 starts and waits for input UI commands. In case of qt-type, a new window pops up, where detector geometry and trajectories of simulated particles may be displayed. If this interactive mode is used, the user is responsible of starting the the simulation (or the visualization) via dedicated UI commands.
The type of interface is setup by the argument 'runType', e.g.,
```
# batch, run, qt, shell
SIM.runType = "batch"
```

Geant4 UI commands can be passed to ddsim in a macrofile, for example visualization commands (to be used together with `runType qt`) can be passed as
```
SIM.macroFile = "vis.mac"
```

## Detector geometry

The detector description is given as a DD4hep detector. The only argument that must be provided to ddsim is a XML compact file containing the full description of a detector. This information can be defined in the steeringFile of ddsim or as a command line argument, e.g. `--compactFile ./CLD_o3_v01.xml`

## Physics

Built-in Physics Lists provided by Geant4 can be used, and extended. The following piece of code is part of the ddsim steering file. It basically sets up the Geant4 built-in `FTFP_BERT` Physics Lists, and extends it with Optical Photons and Cerenkov process. Other parameters affecting the physics are defined here, see the full steering file later. Mind that the PhysicstList to use from Geant4 is defined by `SIM.physics.list = "FTFP_BERT"`, not to be mistaken with `SIM.physicsList = None` which allows you to use a homemade PhysicsList.

```
## The name of the Geant4 Physics list.
SIM.physics.list = "FTFP_BERT"

def setupCerenkov(kernel):
        from DDG4 import PhysicsList

        seq = kernel.physicsList()
        cerenkov = PhysicsList(kernel, "Geant4CerenkovPhysics/CerenkovPhys")
        cerenkov.MaxNumPhotonsPerStep = 10
        cerenkov.MaxBetaChangePerStep = 10.0
        cerenkov.TrackSecondariesFirst = False
        cerenkov.VerboseLevel = 0
        cerenkov.enableUI()
        seq.adopt(cerenkov)
        ph = PhysicsList(kernel, "Geant4OpticalPhotonPhysics/OpticalGammaPhys")
        ph.addParticleConstructor("G4OpticalPhoton")
        ph.VerboseLevel = 0
        ph.BoundaryInvokeSD = True
        ph.enableUI()
        seq.adopt(ph)
        return None

SIM.physics.setupUserPhysics(setupCerenkov)
```

## Actions

Actions refers to different parts that happen while running the actual simulation

### Primary particle generation

Primary particle(s) are the particles that are created before starting the simulation of each event. There is no default, so the user must provide a way to generate these primary particles. 

A first option used for development and early performance studies is to shoot particles in the detector without simulating full physics collisions: ddsim provides a built-in particle gun, which can be configured in the steering file (energy, direction, origin position, ...). Note that the energy in this case means total energy of the particle (mass + kinetic energy).
```
SIM.numberOfEvents = 1000
SIM.enableGun = True
SIM.gun.energy = "50*GeV"
SIM.gun.particle = "pi+"
SIM.gun.distribution = "uniform"
SIM.gun.multiplicity = 1
SIM.gun.position = "0 0 0*cm"
```

Geant4 particle gun and General Particle Source (GPS) can be configured via Geant4 macro file, and passed to ddsim in the steering file as
```
SIM.macroFile = "myGPS.mac"
```

For more realistic performance studies or physics analyses, realistic collisions should be simulated with dedicated MC generators such as Pythia, MadGraph, KKMC, Whizard, etc, outside of ddsim. The output of the MC generators can be fed to ddsim under the following formats: .stdhep, .slcio, .HEPEvt, .hepevt, .hepmc, .hepmc3, .hepmc3.tree.root and .pairs. Dedicated options to read the input files can be provided (see full steering file).

### Sensitive detector (SD), SD actions and filters

Sensitive detector actions defines how the Geant4 step information is stored, are instantiated as plugins (the source code can be browsed [here](https://github.com/AIDASoft/DD4hep/blob/master/DDG4/plugins/Geant4SDActions.cpp)), and can be associated to a detector or subdetector (based on its name). For example, the following piece of code associates the Action named `Geant4OpticalTrackerAction` with the subdetectors named `ARCBARREL` and `ARCENDCAP`
```
SIM.action.mapActions["ARCBARREL"] = "Geant4OpticalTrackerAction"
SIM.action.mapActions["ARCENDCAP"] = "Geant4OpticalTrackerAction"
```

The information to be saved in the output file can be filtered. DD4hep provides some default filters, like `edep0` which forces the saving of the hit information even if the energy was zero. Custom filters can be defined, as in the following piece of code, where the filter named `opticalphotons` is used to output hits by optical photons only.

```
SIM.filter.calo = "edep0"

##  list of filter objects: map between name and parameter dictionary 
#SIM.filter.filters = {'geantino': {'name': 'GeantinoRejectFilter/GeantinoRejector', 'parameter': {}}, 'edep1kev': {'name': 'EnergyDepositMinimumCut', 'parameter': {'Cut': 0.001}}, 'edep0': {'name': 'EnergyDepositMinimumCut/Cut0', 'parameter': {'Cut': 0.0}}}

##  a map between patterns and filter objects, using patterns to attach filters to sensitive detector 
SIM.filter.mapDetFilter = {}

##  default filter for tracking sensitive detectors; this is applied if no other filter is used for a tracker
#SIM.filter.tracker = "edep1kev"
SIM.filter.tracker = "edep0"
# Some detectors are only sensitive to optical photons
SIM.filter.filters["opticalphotons"] = dict(
        name="ParticleSelectFilter/OpticalPhotonSelector",
        parameter={"particle": "opticalphoton"},
        )
SIM.filter.mapDetFilter["ARCBARREL"] = "opticalphotons"
SIM.filter.mapDetFilter["ARCENDCAP"] = "opticalphotons"
```

### Random number generation

In order to ensure reproducibility of the simulation, the option `SIM.random.enableEventSeed = True` must be enabled.

### Custom steering file

The following steering file contains only part of the simulations parameters. 

```python
#steering.py 
from DDSim.DD4hepSimulation import DD4hepSimulation
SIM = DD4hepSimulation()

# Register optical physics and Cerenkov process to the default physics
def setupCerenkov(kernel):
        from DDG4 import PhysicsList

        seq = kernel.physicsList()
        cerenkov = PhysicsList(kernel, "Geant4CerenkovPhysics/CerenkovPhys")
        cerenkov.MaxNumPhotonsPerStep = 10
        cerenkov.MaxBetaChangePerStep = 10.0
        cerenkov.TrackSecondariesFirst = False
        cerenkov.VerboseLevel = 0
        cerenkov.enableUI()
        seq.adopt(cerenkov)
        ph = PhysicsList(kernel, "Geant4OpticalPhotonPhysics/OpticalGammaPhys")
        ph.addParticleConstructor("G4OpticalPhoton")
        ph.VerboseLevel = 0
        ph.BoundaryInvokeSD = True
        ph.enableUI()
        seq.adopt(ph)
        return None
SIM.physics.setupUserPhysics(setupCerenkov)

# Associate the Geant4OpticalTrackerAction to these detectors
# this action register total energy of the opt photon as a single hit
# and kills the optical photon, so no time is wasted tracking them
SIM.action.mapActions["ARCBARREL"] = "Geant4OpticalTrackerAction"
SIM.action.mapActions["ARCENDCAP"] = "Geant4OpticalTrackerAction"

# Register hit with low energy, compatible with zero
SIM.filter.tracker = "edep0"

# Define filter, so detector is only sensitive to optical photons
SIM.filter.filters["opticalphotons"] = dict(
        name="ParticleSelectFilter/OpticalPhotonSelector",
        parameter={"particle": "opticalphoton"},
        )
SIM.filter.mapDetFilter["ARCBARREL"] = "opticalphotons"
SIM.filter.mapDetFilter["ARCENDCAP"] = "opticalphotons"

# Particle gun settings: pions with fixed energy, random direction
SIM.numberOfEvents = 1000
SIM.enableGun = True
SIM.gun.energy = "50*GeV"
SIM.gun.particle = "pi+"
SIM.gun.distribution = "uniform"
SIM.gun.multiplicity = 1
SIM.gun.position = "0 0 0"
```

## Full default steering file

```python
from DDSim.DD4hepSimulation import DD4hepSimulation
from g4units import mm, GeV, MeV
SIM = DD4hepSimulation()

## The compact XML file, or multiple compact files, if the last one is the closer.
SIM.compactFile = []
## Lorentz boost for the crossing angle, in radian!
SIM.crossingAngleBoost = 0.0
SIM.enableDetailedShowerMode = False
SIM.enableG4GPS = False
SIM.enableG4Gun = False
SIM.enableGun = False
## InputFiles for simulation .stdhep, .slcio, .HEPEvt, .hepevt, .hepmc, .hepmc3, .hepmc3.tree.root, .pairs files are supported
SIM.inputFiles = []
## Macro file to execute for runType 'run' or 'vis'
SIM.macroFile = ""
## number of events to simulate, used in batch mode
SIM.numberOfEvents = 0
## Outputfile from the simulation: .slcio, edm4hep.root and .root output files are supported
SIM.outputFile = "dummyOutput.slcio"
## Physics list to use in simulation
SIM.physicsList = None
## Verbosity use integers from 1(most) to 7(least) verbose
## or strings: VERBOSE, DEBUG, INFO, WARNING, ERROR, FATAL, ALWAYS
SIM.printLevel = 3
## The type of action to do in this invocation
## batch: just simulate some events, needs numberOfEvents, and input file or gun
## vis: enable visualisation, run the macroFile if it is set
## qt: enable visualisation in Qt shell, run the macroFile if it is set
## run: run the macroFile and exit
## shell: enable interactive session
SIM.runType = "batch"
## Skip first N events when reading a file
SIM.skipNEvents = 0
## Steering file to change default behaviour
SIM.steeringFile = None
## FourVector of translation for the Smearing of the Vertex position: x y z t
SIM.vertexOffset = [0.0, 0.0, 0.0, 0.0]
## FourVector of the Sigma for the Smearing of the Vertex position: x y z t
SIM.vertexSigma = [0.0, 0.0, 0.0, 0.0]


################################################################################
## Helper holding sensitive detector actions.
## 
##   The default tracker and calorimeter actions can be set with
## 
##   >>> SIM = DD4hepSimulation()
##   >>> SIM.action.tracker=('Geant4TrackerWeightedAction', {'HitPositionCombination': 2, 'CollectSingleDeposits': False})
##   >>> SIM.action.calo = "Geant4CalorimeterAction"
## 
##   The default sensitive actions for calorimeters and trackers are applied based on the sensitive type.
##   The list of sensitive types can be changed with
## 
##   >>> SIM = DD4hepSimulation()
##   >>> SIM.action.trackerSDTypes = ['tracker', 'myTrackerSensType']
##   >>> SIM.calor.calorimeterSDTypes = ['calorimeter', 'myCaloSensType']
## 
##   For specific subdetectors specific sensitive detectors can be set based on patterns in the name of the subdetector.
## 
##   >>> SIM = DD4hepSimulation()
##   >>> SIM.action.mapActions['tpc'] = "TPCSDAction"
## 
##   and additional parameters for the sensitive detectors can be set when the map is given a tuple
## 
##   >>> SIM = DD4hepSimulation()
##   >>> SIM.action.mapActions['ecal'] =( "CaloPreShowerSDAction", {"FirstLayerNumber": 1} )
## 
##    
################################################################################

##  set the default calorimeter action 
SIM.action.calo = "Geant4ScintillatorCalorimeterAction"

## List of patterns matching sensitive detectors of type Calorimeter.
SIM.action.calorimeterSDTypes = ['calorimeter']

## Create a map of patterns and actions to be applied to sensitive detectors.
## 
##     Example: if the name of the detector matches 'tpc' the TPCSDAction is used.
## 
##       SIM.action.mapActions['tpc'] = "TPCSDAction"
##     
SIM.action.mapActions = {}
SIM.action.mapActions["ARCBARREL"] = "Geant4OpticalTrackerAction"
SIM.action.mapActions["ARCENDCAP"] = "Geant4OpticalTrackerAction"


##  set the default tracker action 
SIM.action.tracker = ('Geant4TrackerWeightedAction', {'HitPositionCombination': 2, 'CollectSingleDeposits': False})

## List of patterns matching sensitive detectors of type Tracker.
SIM.action.trackerSDTypes = ['tracker']


################################################################################
## Configuration for the magnetic field (stepper) 
################################################################################
SIM.field.delta_chord = 0.25
SIM.field.delta_intersection = 0.001
SIM.field.delta_one_step = 0.01
SIM.field.eps_max = 0.001
SIM.field.eps_min = 5e-05
SIM.field.equation = "Mag_UsualEqRhs"
SIM.field.largest_step = 10000.0
SIM.field.min_chord_step = 0.01
SIM.field.stepper = "ClassicalRK4"


################################################################################
## Configuration for sensitive detector filters
## 
##   Set the default filter for 'tracker'
##   >>> SIM.filter.tracker = "edep1kev"
##   Use no filter for 'calorimeter' by default
##   >>> SIM.filter.calo = ""
## 
##   Assign a filter to a sensitive detector via pattern matching
##   >>> SIM.filter.mapDetFilter['FTD'] = "edep1kev"
## 
##   Or more than one filter:
##   >>> SIM.filter.mapDetFilter['FTD'] = ["edep1kev", "geantino"]
## 
##   Don't use the default filter or anything else:
##   >>> SIM.filter.mapDetFilter['TPC'] = None ## or "" or []
## 
##   Create a custom filter. The dictionary is used to instantiate the filter later on
##   >>> SIM.filter.filters['edep3kev'] = dict(name="EnergyDepositMinimumCut/3keV", parameter={"Cut": 3.0*keV} )
## 
##    
################################################################################

## 
##     default filter for calorimeter sensitive detectors;
##     this is applied if no other filter is used for a calorimeter
##     
SIM.filter.calo = "edep0"

##  list of filter objects: map between name and parameter dictionary 
#SIM.filter.filters = {'geantino': {'name': 'GeantinoRejectFilter/GeantinoRejector', 'parameter': {}}, 'edep1kev': {'name': 'EnergyDepositMinimumCut', 'parameter': {'Cut': 0.001}}, 'edep0': {'name': 'EnergyDepositMinimumCut/Cut0', 'parameter': {'Cut': 0.0}}}

##  a map between patterns and filter objects, using patterns to attach filters to sensitive detector 
SIM.filter.mapDetFilter = {}

##  default filter for tracking sensitive detectors; this is applied if no other filter is used for a tracker
#SIM.filter.tracker = "edep1kev"
SIM.filter.tracker = "edep0"
# Some detectors are only sensitive to optical photons
SIM.filter.filters["opticalphotons"] = dict(
        name="ParticleSelectFilter/OpticalPhotonSelector",
        parameter={"particle": "opticalphoton"},
        )
SIM.filter.mapDetFilter["ARCBARREL"] = "opticalphotons"
SIM.filter.mapDetFilter["ARCENDCAP"] = "opticalphotons"

################################################################################
## Configuration for the GuineaPig InputFiles 
################################################################################

## Set the number of pair particles to simulate per event.
##     Only used if inputFile ends with ".pairs"
##     If "-1" all particles will be simulated in a single event
##     
SIM.guineapig.particlesPerEvent = "-1"


################################################################################
## Configuration for the DDG4 ParticleGun 
################################################################################

SIM.numberOfEvents = 1000
SIM.enableGun = True
SIM.gun.energy = "50*GeV"
SIM.gun.particle = "pi+"
SIM.gun.distribution = "uniform"
SIM.gun.multiplicity = 1
SIM.gun.position = "0 0 0*cm"


################################################################################
## Configuration for the hepmc3 InputFiles 
################################################################################

## Set the name of the attribute contraining color flow information index 0.
SIM.hepmc3.Flow1 = "flow1"

## Set the name of the attribute contraining color flow information index 1.
SIM.hepmc3.Flow2 = "flow2"

## Set to false if the input should be opened with the hepmc2 ascii reader.
## 
##     If ``True`` a  '.hepmc' file will be opened with the HEPMC3 Reader Factory.
## 
##     Defaults to true if DD4hep was build with HEPMC3 support.
##     
SIM.hepmc3.useHepMC3 = False


################################################################################
## Configuration for Input Files. 
################################################################################

## Set one or more functions to configure input steps.
## 
##     The functions must take a ``DD4hepSimulation`` object as their only argument and return the created generatorAction
##     ``gen`` (for example).
## 
##     For example one can add this to the ddsim steering file:
## 
##       def exampleUserPlugin(dd4hepSimulation):
##         '''Example code for user created plugin.
## 
##         :param DD4hepSimulation dd4hepSimulation: The DD4hepSimulation instance, so all parameters can be accessed
##         :return: GeneratorAction
##         '''
##         from DDG4 import GeneratorAction, Kernel
##         # Geant4InputAction is the type of plugin, Cry1 just an identifier
##         gen = GeneratorAction(Kernel(), 'Geant4InputAction/Cry1' , True)
##         # CRYEventReader is the actual plugin, steeringFile its constructor parameter
##         gen.Input = 'CRYEventReader|' + 'steeringFile'
##         # we can give a dictionary of Parameters that has to be interpreted by the setParameters function of the plugin
##         gen.Parameters = {'DataFilePath': '/path/to/files/data'}
##         gen.enableUI()
##         return gen
## 
##       #SIM.inputConfig.userInputPlugin = exampleUserPlugin
## 
##     Repeat function definition and assignment to add multiple input steps
## 
##     
#SIM.inputConfig.userInputPlugin = []


################################################################################
## Configuration for the generator-level InputFiles 
################################################################################

## Set the name of the collection containing the MCParticle input.
##     Default is "MCParticle".
##     
SIM.lcio.mcParticleCollectionName = "MCParticle"


################################################################################
## Configuration for the LCIO output file settings 
################################################################################

## The event number offset to write in slcio output file. E.g setting it to 42 will start counting events from 42 instead of 0
SIM.meta.eventNumberOffset = 0

## Event parameters to write in every event. Use C/F/I ids to specify parameter type. E.g parameterName/F=0.42 to set a float parameter
SIM.meta.eventParameters = []

## The run number offset to write in slcio output file. E.g setting it to 42 will start counting runs from 42 instead of 0
SIM.meta.runNumberOffset = 0


################################################################################
## Configuration for the output levels of DDG4 components 
################################################################################

## Output level for input sources
SIM.output.inputStage = 3

## Output level for Geant4 kernel
SIM.output.kernel = 3

## Output level for ParticleHandler
SIM.output.part = 3

## Output level for Random Number Generator setup
SIM.output.random = 6


################################################################################
## Configuration for Output Files. 
################################################################################

## Set a function to configure the outputFile.
## 
##     The function must take a ``DD4hepSimulation`` object as its only argument and return ``None``.
## 
##     For example one can add this to the ddsim steering file:
## 
##       def exampleUserPlugin(dd4hepSimulation):
##         '''Example code for user created plugin.
## 
##         :param DD4hepSimulation dd4hepSimulation: The DD4hepSimulation instance, so all parameters can be accessed
##         :return: None
##         '''
##         from DDG4 import EventAction, Kernel
##         dd = dd4hepSimulation  # just shorter variable name
##         evt_root = EventAction(Kernel(), 'Geant4Output2ROOT/' + dd.outputFile, True)
##         evt_root.HandleMCTruth = True or False
##         evt_root.Control = True
##         if not dd.outputFile.endswith(dd.outputConfig.myExtension):
##           output = dd.outputFile + dd.outputConfig.myExtension
##         evt_root.Output = output
##         evt_root.enableUI()
##         Kernel().eventAction().add(evt_root)
##         return None
## 
##       SIM.outputConfig.userOutputPlugin = exampleUserPlugin
##       # arbitrary options can be created and set via the steering file or command line
##       SIM.outputConfig.myExtension = '.csv'
##     
SIM.outputConfig.userOutputPlugin = None


################################################################################
## Configuration for the Particle Handler/ MCTruth treatment 
################################################################################

## Enable lots of printout on simulated hits and MC-truth information
SIM.part.enableDetailedHitsAndParticleInfo = False

##  Keep all created particles 
SIM.part.keepAllParticles = False

## Minimal distance between particle vertex and endpoint of parent after
##     which the vertexIsNotEndpointOfParent flag is set
##     
SIM.part.minDistToParentVertex = 2.2e-14

## MinimalKineticEnergy to store particles created in the tracking region
SIM.part.minimalKineticEnergy = 1.0

##  Printout at End of Tracking 
SIM.part.printEndTracking = False

##  Printout at Start of Tracking 
SIM.part.printStartTracking = False

## List of processes to save, on command line give as whitespace separated string in quotation marks
SIM.part.saveProcesses = ['Decay']

## Optionally enable an extended Particle Handler
SIM.part.userParticleHandler = "Geant4TCUserParticleHandler"


################################################################################
## Configuration for the PhysicsList 
################################################################################

## If true, add decay processes for all particles.
## 
##     Only enable when creating a physics list not based on an existing Geant4 list!
##     
SIM.physics.decays = False

## The name of the Geant4 Physics list.
SIM.physics.list = "FTFP_BERT"

##  location of particle.tbl file containing extra particles and their lifetime information
## 
##     For example in $DD4HEP/examples/DDG4/examples/particle.tbl
##     
SIM.physics.pdgfile = None

##  The global geant4 rangecut for secondary production
## 
##     Default is 0.7 mm as is the case in geant4 10
## 
##     To disable this plugin and be absolutely sure to use the Geant4 default range cut use "None"
## 
##     Set printlevel to DEBUG to see a printout of all range cuts,
##     but this only works if range cut is not "None"
##     
SIM.physics.rangecut = 0.7

## Set of PDG IDs that will not be passed from the input record to Geant4.
## 
##     Quarks, gluons and W's Z's etc should not be treated by Geant4
##     
SIM.physics.rejectPDGs = {3201, 1, 3203, 2, 4101, 3, 4103, 4, 5, 6, 21, 23, 24, 5401, 25, 2203, 5403, 3101, 3103, 4403, 2101, 5301, 2103, 5303, 4301, 1103, 4303, 5201, 5203, 3303, 4201, 4203, 5101, 5103, 5503}

## Set of PDG IDs for particles that should not be passed to Geant4 if their properTime is 0.
## 
##     The properTime of 0 indicates a documentation to add FSR to a lepton for example.
##     
SIM.physics.zeroTimePDGs = {17, 11, 13, 15}

def setupCerenkov(kernel):
        from DDG4 import PhysicsList

        seq = kernel.physicsList()
        cerenkov = PhysicsList(kernel, "Geant4CerenkovPhysics/CerenkovPhys")
        cerenkov.MaxNumPhotonsPerStep = 10
        cerenkov.MaxBetaChangePerStep = 10.0
        cerenkov.TrackSecondariesFirst = False
        cerenkov.VerboseLevel = 0
        cerenkov.enableUI()
        seq.adopt(cerenkov)
        ph = PhysicsList(kernel, "Geant4OpticalPhotonPhysics/OpticalGammaPhys")
        ph.addParticleConstructor("G4OpticalPhoton")
        ph.VerboseLevel = 0
        ph.BoundaryInvokeSD = True
        ph.enableUI()
        seq.adopt(ph)
        return None

SIM.physics.setupUserPhysics(setupCerenkov)

################################################################################
## Properties for the random number generator 
################################################################################

## If True, calculate random seed for each event basedon eventID and runID
## Allows reproducibility even whenSkippingEvents
SIM.random.enableEventSeed = True
SIM.random.file = None
SIM.random.luxury = 1
SIM.random.replace_gRandom = True
SIM.random.seed = 1
SIM.random.type = None

```
