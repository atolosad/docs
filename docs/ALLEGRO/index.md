# Introduction

The ALLEGRO (A Lepton coLlider Experiment with highly GRanular calorimetry Read-Out) detector concept features similar sub-detectors as for the IDEA proposal but with different calorimeters: highly granular Noble Liquid ECAL and Iron/scintillator based HCAL. While a detailed implementation of the ECAL barrel together with its reconstruction are already available, the full simulation for this detector is still under construction.

# Detector dimensions

![ALLEGRO_dimensions](img/ALLEGRO_dimensions.png)
<div style="text-align: center"> Layout of the ALLEGRO detector. The precise dimensions are still evolving. </div>
