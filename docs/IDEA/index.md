# Introduction

The IDEA (Innovative Detector for Electron-positron Accelerator) detector concept is based on an ultra-light gaseous drift chamber and a dual-readout calorimeter. The full simulation of this detector will be available soon as many of its sub-detectors are already implemented. A lot of work on its reconstruction still has to be carried on though.

# Detector Dimensions

![IDEA_dimensions](img/IDEA_dimensions.png)
<div style="text-align: center">Schematic layout of the IDEA detector. Disclaimer: the dimensions on this picture are <strong>outdated</strong>.</div>

## Change log
Here is a change log listing recent updates on the baseline geometry which have to be ported to the software implementation: 

- Vertex dimensions changed slightly and should be updated

The dimensions described below are the dimensions as they are/should be implemented in Full Simulation.


## Vertex Detector

The Vertex detector dimensions can be found in the following technical drawing from F. Palla 

![IDEA_vertex_dimensions](img/IDEA_vertex_detector_dimensions.png)

<!-- ### Barrel

| Layer | R [mm] | L [mm] | Si eq. thick. [&mu;m] | X0 [%] | pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|-------|--------|--------|--------------------|--------|-------------------|------------|--------------|
| 1 | 17 | ±110 | 300 | 0.3 | 0.02×0.02 | 235 | 60M |
| 2 | 23 | ±150 | 300 | 0.3 | 0.02×0.02 | 434 | 110M |
| 3 | 31 | ±200 | 300 | 0.3 | 0.02×0.02 | 780 | 200M |
| 4 | 200 | ±2040 | 450 | 0.5 | 0.05×1.0 | 52K | 105M |
| 5 | 220 | ±2240 | 450 | 0.5 | 0.05×1.0 | 62K | 124M |


### Endcap

  

| Disk | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Si eq. thick. [&mu;m] | X0 [%] | pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|-------|----------|-----------|--------|--------------------|--------|-------------------|------------|--------------|
| 1 | 42 | 190 | ±400 | 300 | 0.3 | 0.05×0.05 | 2.2K | 87M |
| 2 | 44 | 190 | ±420 | 300 | 0.3 | 0.05×0.05 | 2.2K | 86M |
| 3 | 78 | 190 | ±760 | 300 | 0.3 | 0.05×0.05 | 1.9K | 76M |
| 4 | 80 | 190 | ±780 | 300 | 0.3 | 0.05×0.05 | 1.9K | 75M | -->

##  Drift Chamber

| Dimensions | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] |
|-------|----------|-----------|-------|
| Drift chamber | 350 | 2000 | ±2000 |
| Service area | 350 | 2000 | ±(2000 to 2250) |

| Key parameter | Value | Comment |
|----------------------------|-----------|----------------------------|
| # of layers | 112 | 14 super layers with 8 layers |
| # of cells | 56448 | 192 at 1st – 816 at last layer |
| Average cell size | 13.9 mm | min 11.8 mm – max 14.9 mm |
| Average stereo angle | 134 mrad | min 43 mrad – max 14.9 mrad |
| Transverse resolution | 100 µm | 80 µm with cluster timing |
| Longitudinal resolution | 750 µm | 600 µm with cluster timing |
| Active volume | 50 m³ | |
| Readout channels | 112 896 |r.o. from both ends|
| max drift time | 400 ns |800 × 8 bit at 2 GHz |


<!--| | inner wall | gas | wires | outer wall | service area |
|-------------|------------|------|-------|------------|--------------|
| thickness [mm] | 0.2 | 1000 | 1000 | 20 | 250 |
| X0 [%] | 0.08 | 0.07 | 0.13 | 1.2 | 4.5 |-->





## Silicon Wrapper

### Barrel
| Layer | R [mm] | L [mm] | Si eq. thick. [&mu;m] | X0[%] | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |
|1 | 2040 | ±2400 | 450 | 0.5 | 0.05×100 |616K | 12.3M |
|2 | 2060 | ±2400 | 450 | 0.5 | 0.05×100 |620K | 12.4M |


### Endcap
| Disk | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Si eq. thick. [&mu;m] | X0[%] | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |--- |
|1 | 350 | 2020 | ±2300 | 450 | 0.5 | 0.05×100 |250K | 5M |
|2 | 354 | 2020 | ±2320 | 450 | 0.5 | 0.05×100 |250K | 5M |

## Magnet

|  | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | X0[%] |
|--- |--- |--- |--- |--- |
|Solenoid | 2100 | 2400 | ±2500 | 0.75 |


## End-plate absorber

|  | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | X0[%] |
|--- |--- |--- |--- |--- |
|Lead | 380 | 2090 | ±(2490 to 2495) | 0.75 |

## Pre-Shower

### Barrel
| Layer | R [mm] | L [mm] | Thickness [mm]| Int. length | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |
|μRwell | 2450 | ±2550 | 20 (two overlapping 10 mm PCB)| | 0.4×500 |785k | 392k |


### Endcap
| Disk | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Thickness [mm]| Int. length | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |--- |
|μRwell | 390 | 2430 | ±2550 | 20 (two overlapping 10 mm PCB)| | 0.4×500 |362k | 181k |


## Dual readout Calorimeter

### Barrel
| R [mm] | L [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |
| 2500 | ±(2600÷4600) | 2000 | 8|


### Endcap
| R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |--- |
| ±(390 to 680)  | ±(2500 to 4500)  | ±(2600 to 4600) | 2000 | 8 |


## Muon Detector and return yoke

### Barrel
| Layer | R [mm] | L [mm] | Thickness [mm]| Int. length | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |
|μRwell | 4520 | ±4500 | 20 (two overlapping 10 mm PCB) | | 1.5×500 |2.6M | 3.5K |
|Iron | 4560 | ±4500 | 300 | | | | |
|μRwell | 4880 | ±4500 | 20 (two overlapping 10 mm PCB)| | 1.5×500 |3.0M | 4.0K |
|Iron | 4920 | ±4500 | 300 | | | | |
|μRwell | 5240 | ±5260 | 20 (two overlapping 10 mm PCB)| | 1.5×500 |4.3M | 5.7K |

### Endcap
| Disk | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Thickness [mm]| Int. length | Pixel size [mm<sup>2</sup>] | area [cm<sup>2</sup>] | # of channels |
|--- |--- |--- |--- |--- |--- |--- |--- |--- |
|μRwell | 700 | 5200 | ±4520 | 20 (two overlapping 10 mm PCB)| | 1.5×500 |1.9M | 2.5K |
|Iron | 700 | 5200 | ±4560 | 300 | | | | |
|μRwell | 700 | 5200 | ±4880 | 20 (two overlapping 10 mm PCB)| | 1.5×500 |1.9M | 2.5K |
|Iron | 700 | 5200 | ±4920 | 300 | | | | |
|μRwell | 700 | 5200 | ±5240 | 20 (two overlapping 10 mm PCB)| | 1.5×500 |1.9M | 2.5K |

# Detector builders

Here is a list of detector builders and their contact person:

| Sub-detector       | Detector builder | Software Contact | Hardware Contact |
|---                 |---               |---             | |
| Vertex detector    | soon             | Arming Ilg     | |
| Drift chamber      | [Link](https://github.com/HEP-FCC/FCCDetectors/blob/main/Detector/DetFCCeeIDEA/src/DriftChamber_o1_v00.cpp) | Brieuc Francois | |
|Silicon wrapper     |   To be written | Armin Ilg | |
|Pre-shower          | To be written | tbd | |
| Dual-readout calorimeter | [Link](https://github.com/HEP-FCC/dual-readout/blob/master/Detector/DRcalo/src/DRgeo.cpp) | tbd | |
| Muon system | [Link](https://github.com/HEP-FCC/FCCDetectors/blob/main/Detector/DetCommon/src/SimpleSensitiveLayeredCylinder_geo.cpp) | Mahmoud Ali | |