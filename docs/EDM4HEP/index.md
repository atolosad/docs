# Manipulating edm4hep objects

Useful ressources:

- [Tutorial](docs/index.md) explaining how to manipulate edm4hep objects
- [Edm4hep object definitions](https://github.com/key4hep/EDM4hep/blob/main/edm4hep.yaml)
- [Edm4hep Doxygen](https://edm4hep.web.cern.ch/index.html)

More content to come: Tracks and track states, ...